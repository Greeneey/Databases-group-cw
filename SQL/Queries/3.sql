SELECT CONCAT(FIRST_NAME, ' ', LAST_NAME) AS "Student", 
(SELECT COUNT(*) FROM GOAL WHERE STUDENT.ID = GOAL.STUDENT_ID) AS "Goals" -- Select total goals for each player
    FROM STUDENT 
        WHERE (SELECT COUNT(*) FROM GOAL WHERE STUDENT.ID = GOAL.STUDENT_ID) > 2; -- Only select if the total goals is more than 2